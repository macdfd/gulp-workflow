// package vars
const pkg = require("./package.json");

// gulp
const gulp = require("gulp");

// load all plugins in "devDependencies" into the variable $
const $ = require("gulp-load-plugins")({
    pattern: ["*"],
    scope: ["devDependencies"]
});

//environments variables: development and production
const development = $.environments.development;
const production = $.environments.production;

const banner = [
    "/**",
    " * @project        <%= pkg.name %>",
    " * @author         <%= pkg.author %>",
    " * @build          " + $.moment().format("llll") + " ET",
    " * @release        " + $.gitRevSync.long() + " [" + $.gitRevSync.branch() + "]",
    " * @copyright      Copyright (c) " + $.moment().format("YYYY") + ", <%= pkg.copyright %>",
    " *",
    " */",
    ""
].join("\n");

const onError = (error) => {
    $.notify.onError({
        message:  "Error: <%= error.message %>",
        sound:    "Beep"
    }) (error);
};

const paths = {

};

// browser-sync tarea para lanzar el server .
gulp.task('browser-sync', () => {
    //watch files
    const files = [
        './**/*.html',
        './assets/css/*.css',
        './assets/js/**/*.js',
        './assets/images/raw/**/*.*'
    ];
 
    //initialize browsersync
    $.browserSync.init(files, {
        server: {
            baseDir: "./"
        }
    });

    /*browserSync.init(files, {
        //browsersync with a php server
        proxy: "seat.local/",
        notify: true
    });*/
});

//favicons-generate task
gulp.task("favicons", () => {
    $.fancyLog("-> Generating favicons");
    return gulp.src('./assets/images/raw/gulp.png').pipe($.favicons({
        appName: pkg.name,
        appDescription: pkg.description,
        developerName: pkg.author,
        background: "#FFFFFF",
        path: "./assets/images/bin/favicon/",    
        display: "standalone",
        orientation: "portrait",
        html: "index.html",
        version: pkg.version,
        logging: false,
        online: false,
        replace: true,
        icons: {
            android: false, // Create Android homescreen icon. `boolean`
            appleIcon: true, // Create Apple touch icons. `boolean`
            appleStartup: false, // Create Apple startup images. `boolean`
            coast: true, // Create Opera Coast icon. `boolean`
            favicons: true, // Create regular favicons. `boolean`
            firefox: true, // Create Firefox OS icons. `boolean`
            opengraph: false, // Create Facebook OpenGraph image. `boolean`
            twitter: false, // Create Twitter Summary Card image. `boolean`
            windows: true, // Create Windows 8 tile icons. `boolean`
            yandex: true // Create Yandex browser icon. `boolean`
        }
    })).pipe(gulp.dest('./assets/images/bin/favicon/'));
});

gulp.task('imagemin', () => {
    $.fancyLog("-> Building Images");
    return gulp.src('./assets/images/raw/**/*.{png,jpg,jpeg,gif,svg}')
        .pipe($.plumber({errorHandler: onError}))
        .pipe($.imagemin({
            progressive: true,
            interlaced: true,
            optimizationLevel: 7,
            svgoPlugins: [{removeViewBox: false}],
            verbose: true,
            use: []
        }))
        .pipe(gulp.dest('./assets/images/bin'))
});

gulp.task('lint', () => {
    $.fancyLog("-> Analyzing js");
    return gulp.src('./assets/js/**/*.js')
        .pipe($.jshint())
});

gulp.task('javascript', ['lint'], () => {
    $.fancyLog("-> Building js");
    return gulp.src('./assets/js/includes/**/*.js')
        .pipe($.plumber({errorHandler: onError}))
        .pipe($.browserify())
        .pipe($.concat('main.js'))
        .pipe(production($.uglify()))
        .pipe(gulp.dest('assets/js/'))
});
 
// Sass task, will run when any SCSS files change & BrowserSync
// will auto-update browsers
gulp.task('css', () => {
    $.fancyLog("-> Building css");
    return gulp.src('./assets/scss/main.scss')
        .pipe($.plumber({errorHandler: onError}))
        .pipe(development($.sourcemaps.init()))
        .pipe($.sass())
        .pipe($.autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(production($.cleanCss()))
        .pipe(production($.header(banner, {pkg: pkg})))
        .pipe(development($.sourcemaps.write()))
        .pipe(gulp.dest('./assets/css/'));
});


gulp.task('inject', ['css', 'javascript'], () => {
    $.fancyLog("-> Injecting css and js");
    const sources = gulp.src(['assets/js/*.js', 'assets/css/*.css']);
    return gulp.src('./index.html')
        .pipe($.inject(sources, { relative:true }))
        .pipe(gulp.dest('./'));
});
 
// Dev task to be run with `gulp`
gulp.task('default', ['imagemin', 'inject', 'browser-sync'], () => {
    gulp.watch("assets/images/raw/**/*.{png,jpg,jpeg,gif,svg}", ['imagemin'])
    gulp.watch("assets/scss/**/*.scss", ['css'])
    gulp.watch("assets/js/**/*.js", ['javascript'])
});

// Prod task to be run with `gulp-build`
gulp.task('build', ['imagemin', 'inject']);