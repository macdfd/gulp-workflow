-------------
Prerequisites
-------------

Node, NPM y Gulp

----------
Installing
----------

npm install

---------------
Run Environment
---------------
gulp --env development
gulp build --env production